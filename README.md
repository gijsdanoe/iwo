# README #

How to use:
1 - In wordcount.py, type which word you want to search for and change the path to the number of month you want to iterate (e.g. if item == 'sportschool': and path = os.path.abspath('2017/12') for december)
2 - Run the program in the terminal, create an output with the number of tweets every hour on each line (python3 wordcount.py > output.txt)
3 - After it's done, run add.py to sum the numbers to get the number of tweets containing your word for that month