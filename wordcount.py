#!/usr/bin/python3

import gzip
import os

def main():
	path = os.path.abspath('2017/12')
	os.chdir(path)
	for filename in os.listdir(path):
		if filename.endswith('.gz'):
			with gzip.open(filename, 'rb') as tweets:
				wordcount = 0
				for line in tweets:
					line = line.lower()
					wordlist = line.decode().split(' ')
					wordlist.pop(0)
					for item in wordlist:
						if item == 'sportschool':
							wordcount += 1
				print(wordcount)
main()
